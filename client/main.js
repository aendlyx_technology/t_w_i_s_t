import '../imports/startup/accounts-config.js';
import '../imports/ui/body.js';

import { Mongo } from 'meteor/mongo';

db = MongoInternals.defaultRemoteCollectionDriver().mongo.db;
collections = db.listCollections();

collections.each(function(n, collection){
  if(collection){
    console.log( collection.name );
  }
});
