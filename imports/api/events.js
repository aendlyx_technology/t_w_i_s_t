import { Mongo } from 'meteor/mongo';

export const Barcelone = new Mongo.Collection('Barcelone');
export const Berlin = new Mongo.Collection('Berlin');
export const Chicago = new Mongo.Collection('Chicago');
export const Ibiza = new Mongo.Collection('Ibiza');
export const London = new Mongo.Collection('London');
export const Los_Angeles = new Mongo.Collection('Los Angeles');
export const Miami = new Mongo.Collection('Miami');
export const New_York = new Mongo.Collection('New York');
export const Paris = new Mongo.Collection('Paris');
export const Rome = new Mongo.Collection('Rome');
export const Shanghai = new Mongo.Collection('Shanghai');
export const Tokyo = new Mongo.Collection('Tokyo');
